require 'rubygems'
require 'sinatra'
require 'erb'
require 'mongo'
require 'sinatra/flash'
require 'pony'

load 'models.rb'
load 'helpers.rb'

enable :sessions

    before do
        @authenticated = authorized?
    end

    get '/' do
        erb :index 
    end

    get '/routes/new' do
        erb :route_new, :layout => false
    end

    get '/public/routes/new' do
        erb :route_new_public, :layout=> false
    end

    get '/routes' do
        require_authentication
        ctx = DataContext.new

        @routes = ctx.get_collection("routes").find({:email => session[:uid]}, {:sort => [['name', :asc]]}) 
        if request.xhr?
            erb :route_list, :layout => false
        else
            erb :route_list 
        end
    end

    post '/routes' do
        if params[:public].nil? or params[:public] == "false"
            require_authentication
            public_route = false
            email = session[:uid]
        else
            public_route = true
            email = ""
        end
        # store route in mongo
        ctx = DataContext.new
       
        route = { :email => email,
            :name => params[:name],
            :points => params[:points],
            :distance => params[:distance],
            :public_route => public_route,
            :created_at => Time.new }
        ctx.insert("routes", route)

        if public_route
            flash[:notice] = "Successfully created public route available at <a href='http://sketchroute.com/routes/#{route[:_id]}'>http://sketchroute.com/routes/#{route[:_id]}</a>."
        else
            flash[:success] = "Successfully created route!"
        end

        redirect '/'
    end

    get '/routes/:id' do
        ctx = DataContext.new
        @route = ctx.get_collection("routes").find_one(:_id => BSON::ObjectID(params[:id]))
        if !@route["public_route"]
            if !authorized? or @route["email"] != session[:uid]
                flash[:notice] = "Could not locate your route!"
                redirect '/'
            end
        end

        if request.xhr?
            erb :routes_show, :layout => false        
        else
            erb :index
        end
    end

    delete '/routes/:id' do
        require_authentication 
        
        ctx = DataContext.new
        @route = ctx.get_collection("routes").find_one(:_id => BSON::ObjectID(params[:id]))
        if @route["email"] != session[:uid]
            flash[:notice] = "Could not locate your route!"
            redirect '/'
        end

        ctx.get_collection("routes").remove(:_id => BSON::ObjectID(params[:id]))

        if request.xhr?
            "Route has been successully deleted!"
        else
            flash[:success] = "Route has been successfully deleted!"
            redirect '/'
        end
    end

    get '/user/new' do
        if request.xhr?
            erb :user_new, :layout => false
        else
            erb :user_new
        end
    end

    post '/user' do
        dataContext = DataContext.new

        if params[:first_name].nil? || params[:email].nil? || params[:password].nil? || params[:email].length == 0 || params[:first_name].length == 0 || params[:password].length == 0
            flash[:error] = "Missing required information for registration, please provide values for each field."
            redirect '/user/new'
        end

        salt = generate_salt params[:email]
        u = {:first_name => params[:first_name],
            :email => params[:email].downcase,
            :password => encrypt(params[:password], salt),
            :salt => salt,
            :created_at => Time.new }

        ctx = DataContext.new
        ctx.insert "users", u

        create_user_session(u[:email])

        flash[:success] = "Congratulations! Your account has been created and you have been logged in."

        redirect '/'
    end

    get '/stats' do
        require_admin
        dataContext = DataContext.new

        @user_count = dataContext.get_collection("users").count()
        @route_count = dataContext.get_collection("routes").count()
        @public_route_count = dataContext.get_collection("routes").find(:public_route => true).count()

        erb :stats
    end

    get '/session/new' do 
        if request.xhr?
            erb :session_new, :layout => false 
        else
            erb :session_new
        end
    end

    post '/session' do
        user = authenticate_user(params[:email], params[:password])

        if !user.nil? 
            # set user session
            create_user_session(user["email"])
            flash[:success] = "You have successfully signed in!"
            redirect '/'
        else
            # error login, redirect with message
            flash[:error] = "Invalid credentials, please try again. If you have not yet registered, please use the registration link above."
            redirect '/session/new'
        end
    end

    get '/session/destroy' do
        destroy_user_session

        redirect '/'
    end

    get '/about' do
        erb :about
    end

    get '/feedback/new' do
        erb :feedback_new, :layout => false
    end

    post '/feedback' do
        Pony.mail(:to => 'adam.miller@bitcohesion.com', :via => :smtp, 
            :subject => 'sketchroute feedback',
            :from => 'feedback@sketchroute.com',
            :body => params[:message],
            :via_options => {
                :address        => 'smtp.sendgrid.net',
                :port           => '25',
                :authentication => :plain,
                :user_name      => ENV['SENDGRID_USERNAME'],
                :password       => ENV['SENDGRID_PASSWORD'],
                :domain         => ENV['SENDGRID_DOMAIN']
            })

        flash[:success] = "Successfully sent feedback!"

        redirect '/'
    end

    get '/features' do
        erb :features
    end

    not_found do
        erb :not_found
    end

    error do
        erb :error
    end

