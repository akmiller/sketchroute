$(document).ready(function() {
    
    var mapConfig = {
        zoom: 14,
        center: new google.maps.LatLng(39.367, -94,361),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var mapElement = document.getElementById('map-canvas');

    var sketchRouteGMap = new SketchRouteGMaps();
    sketchRouteGMap.init(mapElement, mapConfig);

    document.addEventListener("onDraw", function() {
        updateDistance();
    });

    $("#unit-miles,#unit-kilometers").click(function() {
        //force a redraw of the map
        if ($("#unit-miles").attr("checked"))
            sketchRouteGMap.unit = Unit.miles;
        else
            sketchRouteGMap.unit = Unit.kilometers;

        sketchRouteGMap.draw();
    });

    $("#clear-map").click(function(evt) {
        evt.preventDefault();
        if (confirm("Are you sure you want to clear the map?")) {
            sketchRouteGMap.reset();
        }
    });

    $("#undo").click(function(evt) {
        evt.preventDefault();
        sketchRouteGMap.undo();
    });

    updateDistance = function() {
        var unit = Unit.kilometers;
        if ($('#unit-miles').attr('checked'))
            unit = Unit.miles;

        var label = unit == Unit.kilometers ? "km" : "miles";
        var distance = round(sketchRouteGMap.getTotalDistance(unit));
        if (distance == 0)
            $("#total-distance").html("0.000 " + label);
        else
            $("#total-distance").html(distance + " " + label);
    };
});

function round(val) {
    return (Math.round(val*1000)/1000);
}
