MILES_PER_KILOMETER = 0.621371192

var Unit = { "kilometers": 0, "miles": 1 };

function SketchRouteMaps() {

}

SketchRouteMaps.prototype = {

    init: function(mapElement, config, latLngs) {
        this.map = this.createMap(mapElement, config);
        this.line = null;
        this.markers = [];
        this.latLngs = [];
        
        this.distanceMarkers = [];

        this.onDraw = document.createEvent("Event");
        this.onDraw.initEvent("onDraw", true, true);

        this.onMapChanged = document.createEvent("Event");
        this.onMapChanged.initEvent("onMapChanged", true, true);

        if (latLngs != null) {
            this.latLngs = latLngs;
            if (this.latLngs.length > 0) {
                this.addMarker(this.latLngs[0]);
                this.addMarker(this.latLngs[this.latLngs.length-1]);
                this.draw();
                this.setZoom(15);
                this.setCenter(this.latLngs[0]);
            }
        }
    },

    unit: Unit.miles,

    snapToStreet: true,

    followMe: true,

    showDistanceMarkers: true,

    createMap: function(mapElement, config) {
        /* create map for provider */
    },

    addLatLng: function(latLng) {
        if (latLng !== 'undefined')
            this.latLngs.push(latLng);
    },

    addMarker: function(latLng) {
        /* specific to each map implementation */
    },

    draw: function() {
        /* specific to each map impelementation */
    },

    getDistanceMarkerLatLngs: function(distanceBetween, unit) {
        var distanceMarkers = [];
        if (this.latLngs == null || this.latLngs.length == 0)
            return distanceMarkers;

        var startLatLng = this.latLngs[0];
        var distanceSinceLastMarker = 0;

        for (var index = 1, latLng; latLng = this.latLngs[index]; ++index) {
            var distance = parseFloat(startLatLng.distanceTo(latLng, 6));
            if (unit == Unit.miles)
                distance *= MILES_PER_KILOMETER;

            distanceSinceLastMarker += distance;

            var currLatLng = latLng;
            var bearingToStart = currLatLng.bearingTo(startLatLng);
            while(distanceSinceLastMarker > distanceBetween) {
                var distanceToMarker = distanceSinceLastMarker - distanceBetween;
                var markerLatLng = currLatLng.destinationPoint(bearingToStart, distanceToMarker);
                distanceMarkers.push(markerLatLng);
                distanceSinceLastMarker = distanceToMarker;
            }

            startLatLng = latLng;
        }

        return distanceMarkers;
    },

    getTotalDistance: function(unit) {
        if (this.latLngs == null || this.latLngs.length == 0)
            return 0;
        
        var totalDistance = 0;
        for (var i = 1; i < this.latLngs.length; i++)
            totalDistance += parseFloat(this.latLngs[i].distanceTo(this.latLngs[i-1]));

        // if unit is miles then convert
        if (unit != null && unit == Unit.miles)
            totalDistance = totalDistance * MILES_PER_KILOMETER;

        return totalDistance;
    },

    // undo the last point on the map
    undo: function() {
        /* map specific implementation */
    },

    reset: function() {
        /* map specific implementation */
    },

    setZoom: function(zoom) {

    },

    setCenter: function(latLng) {

    },

    getZoom: function() {
    },

    getCenter: function() {
    }

};
