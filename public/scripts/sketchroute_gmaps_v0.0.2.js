
function SketchRouteGMaps() {
    SketchRouteMaps.apply(this, arguments);
}

SketchRouteGMaps.prototype = new SketchRouteMaps();

SketchRouteGMaps.prototype.createMap = function(mapElement, config) {
    var sketchRouteGMap = this;
    var map = new google.maps.Map(mapElement, config);
    this.latLngIndices = [];

    var doubleClicked = false;
    google.maps.event.addListener(map, 'dblclick', function(evt) {
        doubleClicked = true;
    });
    google.maps.event.addListener(map, 'click', function(evt) {
        doubleClicked = false;
        window.setTimeout(function() {
            if (!doubleClicked) {
                var latlng = new LatLon(evt.latLng.lat(), evt.latLng.lng());
                if (sketchRouteGMap.snapToStreet) {
                    if (sketchRouteGMap.latLngs.length > 0) {
                        sketchRouteGMap._getLatLngsForRoute(sketchRouteGMap.latLngs[sketchRouteGMap.latLngs.length-1], latlng);
                        //sketchRouteGMap.addMarker(latlng);
                    } else {
                        sketchRouteGMap.addLatLng(latlng);
                        sketchRouteGMap.addMarker(latlng);
                    }
                } else {
                    sketchRouteGMap.addLatLng(latlng);
                    sketchRouteGMap.addMarker(latlng);
                    sketchRouteGMap.draw();
                }
            }
        }, 250);
    });
    google.maps.event.addListener(map, 'center_changed', function() {
        if (sketchRouteGMap.onMapChanged != null)
            document.dispatchEvent(sketchRouteGMap.onMapChanged);
    });
    google.maps.event.addListener(map, 'zoom_changed', function() {
        // redraw markers where appropriate
        sketchRouteGMap.drawDistanceMarkers(sketchRouteGMap._getDistanceMarkerInterval(), sketchRouteGMap.unit);

        if (sketchRouteGMap.onMapChanged != null)
            document.dispatchEvent(sketchRouteGMap.onMapChanged);
    });

    return map;
};

SketchRouteGMaps.prototype.addMarker = function(latLng) {
    gLatLng = new google.maps.LatLng(latLng._lat, latLng._lon);

    var marker = new google.maps.Marker({
        position: gLatLng,
        map: this.map,
        clickable: false,
        draggable: false 
    });

    if (this.markers.length > 1)
        this.markers[this.markers.length-1].setMap(null);

    // this is used to know which latlngs are associated to given marker
    this.latLngIndices.push(this.latLngs.length-1);
    this.markers.push(marker);

    if (this.followMe)
        this.map.panTo(gLatLng);
};

SketchRouteGMaps.prototype.draw = function() {
    if (this.latLngs == null || this.latLngs.length < 1)
        return;

    var gLatLngs = [];
    for (var i=0; i < this.latLngs.length; i++)
        gLatLngs.push(new google.maps.LatLng(this.latLngs[i]._lat, this.latLngs[i]._lon));

    this.providerLatLngs = gLatLngs;

    // map is drawn at each point so line should be removed
    if (this.line != null) {
        this.line.setMap(null);
        this.line = null;
    }

    this.line = new google.maps.Polyline({
        path: gLatLngs,
        map: this.map,
        strokeColor: "#ff0000",
        strokeOpacity: 0.5,
        clickable: false,
        strokeWeight: 5
    });

    // clear distance markers if there were any
    this.drawDistanceMarkers(this._getDistanceMarkerInterval(), this.unit);

    if (this.onDraw != null)
        document.dispatchEvent(this.onDraw);
};

SketchRouteGMaps.prototype.drawDistanceMarkers = function(distance, unit) {
    if (this.distanceMarkers != null && this.distanceMarkers.length > 0) {
        for (var i=0; i < this.distanceMarkers.length; i++)
            this.distanceMarkers[i].setMap(null);

        this.distanceMarkers = [];
    }

    if (distance == 0 || !this.showDistanceMarkers)
        return;
    
    // get distance markers
    var distanceMarkerLatLngs = this.getDistanceMarkerLatLngs(distance, this.unit);
    for (var i=0; i < distanceMarkerLatLngs.length; i++) {
        var gLatLng = new google.maps.LatLng(distanceMarkerLatLngs[i].lat(), distanceMarkerLatLngs[i].lon());
        var text = ((i+1) * distance).toString();

        var marker = new sketchroute.maps.Label({
            "position": gLatLng,
            "content": text,
            "map": this.map
        });
        
        this.distanceMarkers.push(marker);
    }
};

SketchRouteGMaps.prototype.undo = function() {
    if (this.latLngs == null || this.latLngs.length == 0 || this.markers == null || this.markers.length == 0)
        return;
    
    if (this.markers.length == 1) {
        this.reset();
    } else {
        // remove last marker then back up latlngs to the next last marker
        this.markers[this.markers.length-1].setMap(null);
        this.markers.length--;

        var latLngStartIndex = this.latLngIndices[this.markers.length-1];
        var latLngEndIndex = this.latLngIndices[this.markers.length];
        this.latLngIndices.length--;
        
        var removeCount = latLngEndIndex - latLngStartIndex;
        if (removeCount > 0)
            this.latLngs.length = this.latLngs.length - removeCount;

        this.markers[this.markers.length-1].setMap(this.map);
    }
    this.draw();
};

SketchRouteGMaps.prototype.reset = function() {
    for (var i=0; i < this.markers.length; i++)
        this.markers[i].setMap(null);
    
    if (this.distanceMarkers != null && this.distanceMarkers.length > 0) {
        for (var i=0; i < this.distanceMarkers.length; i++)
            this.distanceMarkers[i].setMap(null);

        this.distanceMarkers = [];
    }

    this.line.setMap(null);

    this.markers = [];
    this.latLngs = [];
    this.line = null;

    this.latLngIndices = [];
    this.providerLatLngs = [];

    if (this.onDraw != null)
        document.dispatchEvent(this.onDraw);
};

SketchRouteGMaps.prototype.setZoom = function(zoomLevel) {
    this.map.setZoom(zoomLevel);
};

SketchRouteGMaps.prototype.setCenter = function(latLng) {
    this.map.setCenter(new google.maps.LatLng(latLng.lat(), latLng.lon()));
};

SketchRouteGMaps.prototype.getZoom = function() {
    return this.map.getZoom();
};

SketchRouteGMaps.prototype.getCenter = function() {
    var center = this.map.getCenter();
    return new LatLon(center.lat(), center.lng());
};

SketchRouteGMaps.prototype._getLatLngsForRoute = function(startLatLng, endLatLng) {
    var sketchRouteGMap = this;
    var directionsService = new google.maps.DirectionsService();
    var request = {
        origin: new google.maps.LatLng(startLatLng.lat(), startLatLng.lon()),
        destination: new google.maps.LatLng(endLatLng.lat(), endLatLng.lon()),
        travelMode: google.maps.DirectionsTravelMode.WALKING
    };
    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            if (response.routes.length > 0) {
                var leg = response.routes[0].legs[0];
                for (var i=0; i < leg.steps.length; i++) {
                    var step = leg.steps[i];
                    for (var y=0; y < step.lat_lngs.length; y++) {
                        var latLng = new LatLon(step.lat_lngs[y].lat(), step.lat_lngs[y].lng());
                        sketchRouteGMap.latLngs.push(latLng);
                    }
                    if (i == leg.steps.length-1)
                        sketchRouteGMap.addMarker(endLatLng);
                }
                sketchRouteGMap.draw();
            }
        }
    });
};

SketchRouteGMaps.prototype._getDistanceMarkerInterval = function() {
    var zoomLevel = this.getZoom();

    if (zoomLevel > 12)
        return 1;

    if (zoomLevel > 11)
        return this.unit == Unit.miles ? 2 : 3;

    if (zoomLevel > 9)
        return this.unit == Unit.miles ? 3 : 5;

    return 0;
};

function compareLatLngs(lat1,lng1, lat2, lng2) {
    if (roundLatLng(lat1) == roundLatLng(lat2) && roundLatLng(lng1) == roundLatLng(lng2))
        return true;
    
    return false;
}

function roundLatLng(val)
{
    return ( Math.round(val * 1000000) / 1000000 );
}


