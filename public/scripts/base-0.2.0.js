var FEET_PER_METER = 3.2808399
google.load('visualization', '1', { 'packages': ['columnchart'] });

var defaultZoom = 4;
var defaultLatLng = new LatLon(39.367, -94.361);
var defaultUnit;
var _totalUp = 0;
var _totalDown = 0;

$(document).ready(function() {
    var mapContent = $('#main-map');

    defaultUnit = getDefaultUnit();
    var centerLatLng = getDefaultLatLng();
    var mapConfig = {
        zoom: getDefaultZoom(),
        center: new google.maps.LatLng(centerLatLng.lat(), centerLatLng.lon()),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
   
    var latLngs = [];
    if (typeof public_route_points !== 'undefined' && public_route_points != null && public_route_points.length > 0) {
        for (var i=0; i < public_route_points.length; i=i+2) {
            latLngs.push(new LatLon(public_route_points[i], public_route_points[i+1]));
        }
    }

    var mapElement = document.getElementById('map-canvas');
    if (mapElement != null) {
        var sketchRouteGMap = new SketchRouteGMaps();
        sketchRouteGMap.snapToStreet = getDefaultDrawMode();

        if (defaultUnit == "miles")
            sketchRouteGMap.unit = Unit.miles;
        else {
            sketchRouteGMap.unit = Unit.kilometers;
            $('#total-distance').html("0.000 km");
        }
        // add event before initiating map so distance gets drawn
        document.addEventListener("onDraw", function() {
            updateDistance();
            drawElevation();
        }, false); 
        // map change update cookies
        document.addEventListener("onMapChanged", function() {
            var center = sketchRouteGMap.getCenter();
            setCookie('location', center.lat() + ',' + center.lon(), new Date(2040,1,1));
            setCookie('zoom', sketchRouteGMap.getZoom().toString(), new Date(2040,1,1));
        }, false);

        sketchRouteGMap.init(mapElement, mapConfig, latLngs);
       
        //geocoder needed for jump to location
        var geocoder = new google.maps.Geocoder();
        // elevator for drawing graph
        var elevator = new google.maps.ElevationService(); 

        $(window).resize(function() {
            onResize();
        });
    }

    var getStarted = getCookie('new_user');
    if (getStarted == null || getStarted.length == 0)
        $('#getting-started').show();

    $('#close-getting-started').click(function() {
        $('#getting-started').hide('fast');
        setCookie('new_user', 'false', new Date(2040,1,1));
    });

    function onResize() {
        if (mapContent == null)
            return;

        var height = $(window).height() - 260;
        if (height > 500)
            mapContent.css({'height': $(window).height() - 260 });

        drawElevation();
    }

    // force call onload
    if (mapElement != null)
        onResize();
    
    function codeAddress(address) {
        geocoder.geocode( { 'address': address }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                var gLatLng = results[0].geometry.location;
                var latLng = new LatLon(gLatLng.lat(), gLatLng.lng());
                sketchRouteGMap.setCenter(latLng);
                sketchRouteGMap.setZoom(15);
            }
            else
                alert("Could not locate address. " + status);
        });
    }
    
    function drawElevation() {
        var locations = sketchRouteGMap.providerLatLngs;
        if (locations == null || locations.length < 2 ) {
            $('#elevation').hide();
            $('#ads').show();
            return;
        }
        
        $('#ads').hide();
        var elevation = $('#elevation');
        var chartElement = document.getElementById('elevation_chart');
        if (chartElement == null)
            return;

        chart = new google.visualization.ColumnChart(chartElement);

        var pathRequest = {
            'path': locations,
            'samples': 256
        }

        if (locations.length < 2) {
            elevation.hide('fast');
            return;
        }
        else
            elevation.show('fast');

        elevator.getElevationAlongPath(pathRequest, function(results, status) {
            if (status == google.maps.ElevationStatus.OK) {
                elevations = results;

                var totalElevation = calculateElevation(elevations);
                var elevationUnitLabel = "m";
                if (sketchRouteGMap.unit == Unit.miles) {
                    totalElevation = toFeet(totalElevation);
                    _totalUp = toFeet(_totalUp);
                    _totalDown = toFeet(_totalDown);
                    elevationUnitLabel = "ft";
                } else {
                    totalElevation = round(totalElevation);
                    _totalUp = round(_totalUp);
                    _totalDown = round(_totalDown);
                }

                $("#elevation_title").html("Route Elevation (" + elevationUnitLabel + ")");
                $("#elevation_info").html("Total: " + totalElevation +  elevationUnitLabel);
                $("#elevation_stats").html("Uphill: " + _totalUp + elevationUnitLabel + "  /  Downhill: " + _totalDown + elevationUnitLabel);

                var elevationPath = [];
                for (var i = 0; i < results.length; i++)
                    elevationPath.push(elevations[i].location);

                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Sample');
                data.addColumn('number', 'Elevation');
                if (sketchRouteGMap.unit == Unit.miles) {
                    for (var i = 0; i < results.length; i++) {
                        data.addRow(['', toFeet(elevations[i].elevation)]);
                    }
                } else {
                    for (var i = 0; i < results.length; i++) {
                        data.addRow(['', elevations[i].elevation]);
                    }
                }

                var chartWidth = $('#map-options').width();
                elevation.show('fast');
                chart.draw(data, {
                    width: chartWidth,
                    height: 150,
                    legend: 'none',
                    titleY: ''
                });
            }
        });
    }

    $("#unit-miles,#unit-kilometers").click(function() {
        //force a redraw of the map
        if ($("#unit-miles").attr("checked"))
            sketchRouteGMap.unit = Unit.miles;
        else
            sketchRouteGMap.unit = Unit.kilometers;

        unit = sketchRouteGMap.unit == Unit.miles ? "miles" : "km";
        setCookie("unit", unit, new Date(2040,1,1));

        sketchRouteGMap.draw();

        if (sketchRouteGMap.getTotalDistance() == 0)
            $('#total-distance').html("0.000 " + unit);
    });

    $("#draw-freeform,#draw-snaptostreet").click(function() {
        var snap = $("#draw-snaptostreet").attr('checked');
        sketchRouteGMap.snapToStreet = snap;
        setCookie("drawmode", snap ? "snap" : "ff", new Date(2040,1,1));
    });

    $("#clear-map").click(function(evt) {
        evt.preventDefault();
        if (confirm("Are you sure you want to clear the map?")) {
            sketchRouteGMap.reset();
            $('#elevation').hide('fast');
        }
    });

    $("#undo").click(function(evt) {
        evt.preventDefault();
        sketchRouteGMap.undo();
    });

    function updateDistance() {
        var unit = Unit.kilometers;
        if ($('#unit-miles').attr('checked'))
            unit = Unit.miles;

        var label = unit == Unit.kilometers ? "km" : "miles";
        var distance = round(sketchRouteGMap.getTotalDistance(unit));
        if (distance == 0)
            $("#total-distance").html("0.000 " + label);
        else
            $("#total-distance").html(distance + " " + label);
    };

    $('#login').click(function(e) {
        e.preventDefault();

        $.get($(this).attr('href'), function(data) {
            var $modal = $(document.createElement('div')).attr('class','modal').html(data);
            $modal.modal();

            $('#cancel_login').click(function(e) {
                e.preventDefault(); 
                $.modal.close(); 
            });
        });
    });

    $('#register').click(function(e) {
        e.preventDefault();

        $.get($(this).attr('href'), function(data) {
            var $modal = $(document.createElement('div')).attr('class', 'modal').html(data);
            $modal.modal();

            $('#cancel_register').click(function(e) {
                e.preventDefault();
                $.modal.close();
            });
        });
    });

    $('#feedback').click(function(e) {
        e.preventDefault();

        $.get($(this).attr('href'), function(data) {
            var $modal = $(document.createElement('div')).attr('class', 'modal').html(data);
            $modal.modal();
        });
    });

    $(".delete-route").live('click', function(e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('href'),
            type: 'DELETE',
            beforeSend: function(req) {
                return confirm("Are you sure you want to delete this route?");
            },
            success: function(data) {
                $.modal.close();
                $('#user-messages').append($(document.createElement('div')).attr('id','success').html(data));
            }
        });
    });

    $('#my-routes').click(function(e) {
        e.preventDefault();

        $.get($(this).attr('href') + '?time=' + new Date().getTime().toString(), function(data) {
            var $modal = $(document.createElement('div')).attr('class','modal').html(data);
            $modal.modal();

            $('#close').click(function(e) {
                e.preventDefault();
                $.modal.close();
            });
        });
    });
    
    $('#feedback').click(function(e) {
        e.preventDefault();

        $.get($(this).attr('href'), function(data) {
            var $modal = $(document.createElement('div')).attr('class', 'modal').html(data);
            $modal.modal();
        });
    });

    $('#goto-location').click(function(event) {
        event.preventDefault();
        var address = $('#location').val();
        if (address.length > 0)
            codeAddress(address);
    });

    $('#location').keydown(function (event) {
        if (event.which == 13)
            $('#goto-location').click();
    });

    $(".cancel-modal").live('click', function(e) {
        e.preventDefault();
        $.modal.close();
    });

    $('#save-route').click(function(e) {
        e.preventDefault();

        if (sketchRouteGMap.latLngs.length < 2) {
            alert("Must create a route before it can be saved!");
            return (false);
        }

        $.get($(this).attr('href'), function(data) {
            var $modal = $(document.createElement('div')).attr('class','modal').html(data);
            $modal.modal();

            $('#cancel_save').click(function(e) {
                e.preventDefault();
                $.modal.close();
            });

            var stops = [];
            for (var i=0; i < sketchRouteGMap.latLngs.length; i++) {
                stops.push([sketchRouteGMap.latLngs[i].lat(), sketchRouteGMap.latLngs[i].lon()]);
            }

            //storing in miles for now
            var distance = sketchRouteGMap.getTotalDistance(Unit.miles);

            $('#points').val(stops.toString());
            $('#distance').val(round(distance));
            $('#distance-label').html($('#total-distance').html());
        });
    });

    $('#save-public-route').click(function(e) {
        e.preventDefault();

        if (sketchRouteGMap.latLngs.length < 2) {
            alert("Must create a route before it can be saved!");
            return (false);
        }

        $.get($(this).attr('href'), function(data) {
            var $modal = $(document.createElement('div')).attr('class','modal').html(data);
            $modal.modal();

            $('#cancel_save').click(function(e) {
                e.preventDefault();
                $.modal.close();
            });

            var stops = [];
            for (var i=0; i < sketchRouteGMap.latLngs.length; i++) {
                stops.push([sketchRouteGMap.latLngs[i].lat(), sketchRouteGMap.latLngs[i].lon()]);
            }

            var distance = sketchRouteGMap.getTotalDistance();
            if (sketchRouteGMap.unit == Unit.kilometers)
                distance *= MILES_PER_KILOMETER;

            $('#points').val(stops.toString());
            $('#distance').val(round(distance));
            $('#distance-label').html($('#total-distance').html());
        });
    });
});

function calculateElevation(elevations) {
    if (elevations.length == 0)
        return 0;

    var totalElevation = 0;
    var totalUp = 0;
    var totalDown = 0;
    for (var i = 1; i < elevations.length; i++) {
        var startElevation = elevations[i-1].elevation;
        var endElevation = elevations[i].elevation;
        
        var result = (endElevation - startElevation);
        if (result < 0)
            totalDown += result;
        else
            totalUp += result;

        // hack to write total up and down to elevation_stats, should go somewhere more appropriate
        _totalUp = totalUp;
        _totalDown = totalDown;

        totalElevation += result;
    }
  
    return (totalElevation);
    //alert(totalElevation);
}

function getDefaultZoom() {
    var userZoom = getCookie('zoom');
    if (userZoom == null || userZoom.length == 0)
        return defaultZoom;

    return parseFloat(userZoom);
}

function getDefaultLatLng() {
    var userLatLng = getCookie('location');
    if (userLatLng == null || userLatLng.length == 0)
        return defaultLatLng;

    var points = userLatLng.split(',');
    if (isNaN(points[0]) || isNaN(points[1]) || points[0].length == 0 || points[1].length == 0)
        return defaultLatLng;

    var lat, lng;
    try {
        lat = parseFloat(points[0]);
        lng = parseFloat(points[1]);
    }
    catch(err) {
        return defaultLatLng;
    }

    return new LatLon(lat, lng);
}

function getDefaultUnit() {
    var unit = getCookie('unit');
    if (unit == null || unit.length == 0) 
        unit = 'miles'

    if (unit == 'miles') {
        $('#unit-miles').attr('checked', 'checked');
        $('#unit-kilometers').attr('checked', '');
    } else {
        $('#unit-miles').attr('checked', '');
        $('#unit-kilometers').attr('checked', 'checked');
    }

    return unit; 
}

function getDefaultDrawMode() {
    var drawMode = getCookie('drawmode');
    if (drawMode == null || drawMode.length == 0 || drawMode == 'snap') {
        $("#draw-snaptostreet").attr('checked', 'checked');
        $("#draw-freeform").attr('checked', '');
        return true;
    }

    return false;
}

function setCookie(name, val, expires) {
    var cookie = name + "=" + val + ";";
    if (expires != null)
        cookie += " expires=" + expires.toGMTString() + ";";
    cookie += " path=/";
    document.cookie = cookie;
}

function getCookie(name) {
    if (document.cookie.length > 0) {
        start = document.cookie.indexOf(name + "=");
        if (start != -1) {
            start += name.length + 1;
            end = document.cookie.indexOf(";", start);
            if (end == -1) end = document.cookie.length;

            return unescape(document.cookie.substring(start, end));
        }
    }
}

function round(val) {
    return (Math.round(val*1000)/1000);
}

function toFeet(val_in_meters) {
    return round(val_in_meters * FEET_PER_METER);
}

function GUnload() {

}
