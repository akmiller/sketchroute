require 'digest'
require 'json'
require 'uri'

class MongoModel
    attr_accessor :_id

    def new?
        self._id.nil?
    end
end

class DataContext

    def initialize
        if ENV['MONGOHQ_URL'].nil?
            @db = Mongo::Connection.new.db("routemaker")
        else
            uri = URI.parse(ENV['MONGOHQ_URL'])
            @conn = Mongo::Connection.from_uri(ENV['MONGOHQ_URL'])
            @db = @conn.db(uri.path.gsub(/^\//, ''))
        end
    end

    def get_collection(name)
        @db.collection(name)
    end
    
    def insert(collection_name, hash)
        @db.collection(collection_name).insert hash
    end
end

