require 'digest'

helpers do
    def require_authentication
        unless authorized?
            throw(:halt, [400, "Not Authorized"])
        end
    end

    def require_admin
        unless isadmin?
            throw(:halt, [403, "Forbidden"])
        end
    end

    def authorized?
        puts session.inspect
        !session.nil? and !session.has_key?("uid") and session[:uid].to_s.length > 0
    end

    def isadmin?
        #hack for adminstrator right now
        authorized? and session[:uid] == "akmiller@gmail.com"
    end
    
    def generate_salt(email)
        Digest::SHA1.hexdigest("--#{Time.now.to_s}--#{email}--")  
    end

    def encrypt(password, salt)
        Digest::SHA1.hexdigest("#{password}sketchroute#{salt}")
    end
    
    def authenticate_user(email, password) 
        ctx = DataContext.new
        user = ctx.get_collection("users").find_one(:email => email.downcase)
        return nil if user.nil?

        if user["password"] == encrypt(password, user["salt"])
            return user 
        end

        return nil 
    end
    
    def create_user_session(email)
        session[:uid] = email
    end

    def destroy_user_session
        session[:uid] = nil
    end

    def get_user_name
        if !session[:uid].nil?
            ctx = DataContext.new
            user = ctx.get_collection("users").find_one(:email => session[:uid])
            return "" if user.nil?
            return user["first_name"]
        end
        return ""
    end
end
